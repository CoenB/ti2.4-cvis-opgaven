#define _USE_MATH_DEFINES

#include <math.h>
#include <opencv2/opencv.hpp>

#include "blobdetectionavans.h"

#include "utils.h"

void Blob::findBlobs(InputArray src, OutputArray dst, vector<Blob*>& resultArray, int minAreaSize, int maxAreaSize) {
	vector<Point2d*> blobFirstPositions;
	vector<Point2d*> blobCenters;
	vector<int> blobAreas;
	Mat source16bit;
	Mat destination16bit;

	//Convert the image to a 16-bit-signed one.
	src.getMat().convertTo(source16bit, CV_16S);
	//Find the blobs.
	labelBLOBsInfo(source16bit, destination16bit, blobFirstPositions, blobCenters, blobAreas, minAreaSize, maxAreaSize);
	//In the resulting image, every blob has his own (color)value, from 0 to <blob_count>.
	//To make the difference more noticeable, we scale up the range from [0, count] to [0, 255].
	stretch(destination16bit, destination16bit, blobFirstPositions.size());
	//Convert back to 8-bit-unsigned.
	destination16bit.convertTo(dst, CV_8U);

	//Combine the three result arrays into one; more handy :)
	for (int i = 0; i < blobFirstPositions.size(); i++) {
		Point correctedCenter(blobCenters[i]->y, blobCenters[i]->x);
		Blob* blob = new Blob(*blobFirstPositions[i], correctedCenter, blobAreas[i]);
		resultArray.push_back(blob);
	}
}

void Blob::stretch(InputArray src, OutputArray dst, int fromBitCount) {
	Mat source = src.getMat();
	dst.create(source.size(), source.type());
	Mat destination = dst.getMat();
	float scale = 255.0f / fromBitCount;
	for (int row = 0; row < source.rows; row++) {
		for (int col = 0; col < source.cols; col++) {
			short v = source.at<short>(Point(col, row));
			destination.at<short>(Point(col, row)) = v * scale;
		}
	}
}

HSVColor Color::toHsv() const {
	Mat hsv;
	Mat bgr(Size(1, 1), CV_8UC3, *this);
	cvtColor(bgr, hsv, ColorConversionCodes::COLOR_BGR2HSV);
	return HSVColor(hsv.data[0], hsv.data[1], hsv.data[2]);
}

const Color Colors::AZURE = Color(255, 127, 0);
const Color Colors::BLACK = Color(0, 0, 0);
const Color Colors::BLUE = Color(255, 0, 0);
const Color Colors::CHARTREUSE = Color(0, 255, 127);
const Color Colors::CYAN = Color(255, 255, 0);
const Color Colors::GREEN = Color(0, 255, 0);
const Color Colors::MAGENTA = Color(255, 0, 255);
const Color Colors::ORANGE = Color(0, 127, 255);
const Color Colors::RED = Color(0, 0, 255);
const Color Colors::ROSE = Color(127, 0, 255);
const Color Colors::SPRING = Color(127, 255, 0);
const Color Colors::VIOLET = Color(255, 0, 127);
const Color Colors::WHITE = Color(255, 255, 255);
const Color Colors::YELLOW = Color(0, 255, 255);

double Math::areaToRadius(double area) {
	return sqrt(area / M_PI);
}

void Trackbar::clipValue() {
	if (value < minValue)
		value = minValue;
	if (value > maxValue)
		value = maxValue;
}

void Trackbar::setValue(int value) {
	this->value = value;
	clipValue();
	if (callback != nullptr)
		callback(this);
}

void valChanged(int position, void* userData) {
	Trackbar* bar = static_cast<Trackbar*>(userData);
	bar->setValue(position);
}

void Trackbar::setup(String windowName, void (*callback)(Trackbar*)) {
	createTrackbar(name, windowName, &value, maxValue, valChanged, this);
	if (callback != nullptr)
		setOnValueChangedListener(callback);
}