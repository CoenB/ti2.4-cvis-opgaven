#pragma once

#include <opencv2/core.hpp>
#include <vector>

using namespace cv;
using namespace std;

class Blob {
public:
	const int area;
	const Point2d center;
	const Point2d position;

	Blob(Point2d position, Point2d center, int area) :
		position(position),
		center(center),
		area(area) {};

	static void findBlobs(InputArray src, OutputArray dst, vector<Blob*>& resultArray, int minAreaSize = 1, int maxAreaSize = INT_MAX);

private:
	static void stretch(InputArray src, OutputArray dst, int fromBitCount);
};

class HSVColor;

class Color : public Scalar {
public:
	Color(double b, double g, double r) : Scalar(b, g, r) {};

	double r() { return this->val[2]; };
	double g() { return this->val[1]; };
	double b() { return this->val[0]; };
	HSVColor toHsv() const;
};

class HSVColor : public Scalar {
public:
	HSVColor(double h, double s, double v) : Scalar(h, s, v) {};

	double h() { return this->val[0]; };
	void h(double hue) { this->val[0] = hue; };
	double s() { return this->val[1]; };
	void s(double sat) { this->val[1] = sat; };
	double v() { return this->val[2]; };
	void v(double val) { this->val[2] = val; };

	HSVColor withHue(double hue) { return HSVColor(hue, s(), v()); };
	HSVColor withSaturation(double saturation) { return HSVColor(h(), saturation, v()); };
	HSVColor withValue(double value) { return HSVColor(h(), s(), value); };
};

/* Some standard colors */
class Colors {
public:
	static const Color AZURE;
	static const Color BLACK;
	static const Color BLUE;
	static const Color CHARTREUSE;
	static const Color CYAN;
	static const Color GREEN;
	static const Color MAGENTA;
	static const Color ORANGE;
	static const Color RED;
	static const Color ROSE;
	static const Color SPRING;
	static const Color VIOLET;
	static const Color WHITE;
	static const Color YELLOW;
};

class Math {
public:
	static double areaToRadius(double area);
};

class Trackbar {
private:
	void (*callback)(Trackbar* bar) = nullptr;
	int value;

	void clipValue();

public:
	const int maxValue;
	const int minValue;
	const String name;

	Trackbar(String name, int minValue, int maxValue, int value = 0) :
		name(name),
		minValue(minValue),
		maxValue(maxValue),
		value(value) {
		clipValue();
	};

	int getValue() { return value; }
	void setup(String windowName, void (*callback)(Trackbar*) = nullptr);
	void setOnValueChangedListener(void (*callback)(Trackbar*)) {
		this->callback = callback;
	};
	void setValue(int value);
};