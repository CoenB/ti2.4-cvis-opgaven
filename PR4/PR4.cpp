#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>

using namespace cv;
using namespace std;

int main()
{
	VideoCapture capture(0);

	int minH = 0;
	int maxH = 25;

	int minS = 75;
	int maxS = 255;

	int minV = 75;
	int maxV = 255;
	
	if (!capture.isOpened())
	{
		cout << "Could not open the video stream." << endl;
		return 0;
	}

	Mat frame;

	namedWindow("0 - Capture", WINDOW_AUTOSIZE);
	namedWindow("1 - HSV", WINDOW_AUTOSIZE);
	namedWindow("2 - In Range", WINDOW_AUTOSIZE);
	namedWindow("3 - Blurred", WINDOW_AUTOSIZE);
	namedWindow("4 - Dilate", WINDOW_AUTOSIZE);
	namedWindow("5 - Canny", WINDOW_AUTOSIZE);
	namedWindow("6 - Convex", WINDOW_AUTOSIZE);

	while (true)
	{
		if (!capture.read(frame))
		{
			cout << "Cannot read a frame from video stream" << endl;
			break;
		}

		imshow("0 - Capture", frame);

		Mat hsv;
		cvtColor(frame, hsv, CV_BGR2HSV);
		imshow("1 - HSV", hsv);

		Mat range;
		inRange(hsv, Scalar(minH, minS, minV), Scalar(maxH, maxS, maxV), range);
		imshow("2 - In Range", range);

		Mat blurred;
		medianBlur(range, blurred, 5);
		imshow("3 - Blurred", blurred);

		Mat dila;
		Mat element = getStructuringElement(MORPH_ELLIPSE, Size(11, 11), cv::Point(5, 5));
		dilate(blurred, dila, element);
		imshow("4 - Dilate", dila);

		//Mat can;
		//Canny(dila, can, 50, 150);

		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;
		findContours(dila, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

		vector<vector<Point>> largeContours;
		for (int i = 0; i < contours.size(); i++)
		{
			double area = contourArea(contours[i]);
			if (area > 10000)
				largeContours.push_back(contours[i]);
		}

		cout << "Detected " << largeContours.size() << " hands." << endl;

		Mat conv(frame);
		vector<vector<Point>> hull(largeContours.size());
		vector<vector<Point>> approxHull(largeContours.size());
		
		int fingerCount = 0;

		for (int i = 0; i < largeContours.size(); i++)
		{
			convexHull(Mat(largeContours[i]), hull[i]);
			approxPolyDP(hull[i], approxHull[i], 0.015 * arcLength(hull[i], true), true);
			
			drawContours(conv, largeContours, i, Scalar(255, 0, 0), 1);
			//drawContours(conv, hull, i, Scalar(0, 255, 0), 3);
			drawContours(conv, approxHull, i, Scalar(0, 255, 0), 3);

			vector<int> labels;
			cv::partition(hull[i], labels, [](Point p1, Point p2) { return cv::sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y)) < 30; });

			map<int, Point> mapped;
			for (int s = 0; s < hull[i].size(); s++)
				mapped[labels[s]] = hull[i][s];

			cout << mapped.size() << " fingers on hand " << i << endl;
			fingerCount += mapped.size() - 2;

			for (pair<int, Point> p : mapped)
			{
				circle(conv, p.second, 15, Scalar(255, 0, 255), 3);
			}
		}

		putText(conv, to_string(fingerCount) + " fingers", Point(10, 10), FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255));

		//imshow("5 - Canny", can);
		
		//drawContours(conv, con, 0, cv::Scalar(0, 255, 0), 3);
		imshow("6 - Convex", conv);

		int keyCode = waitKey(1);
		if (keyCode == 27)
			break;
	}

	capture.release();
    return 0;
}
