#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

#include "utils.h"

#include "PR1.h"

using namespace cv;
using namespace std;

Mat originImage;
Mat grayImage;
Mat binaryImage;
Mat blobImage;

Trackbar thresholdTrackbar("Threshold", 0, 255, 100);
Trackbar minAreaTrackbar("Minimal size", 0, 20000, 10000);
Trackbar maxAreaTrackbar("Maximal size", 0, 20000, 10000);

void onTrackbarValueChanged(Trackbar* bar) {
	//Convert image to gray.
	cout << "== Original Image --> Grayscale ==" << endl;
	cvtColor(originImage, grayImage, ColorConversionCodes::COLOR_BGR2GRAY);
	imshow("Grayscale", grayImage);

	//Apply a threshold to the image, making it binary.
	cout << "== Grayscale --> Binary (Threshold) ==" << endl;
	threshold(grayImage, binaryImage, thresholdTrackbar.getValue(), 1, ThresholdTypes::THRESH_BINARY_INV);
	imshow("Threshold", binaryImage * 255);

	//Find the blobs.
	cout << "== Binary --> Blobs ==" << endl;
	cout << "Searching Blob's ..." << endl;

	vector<Blob*> blobs;
	Blob::findBlobs(binaryImage, blobImage, blobs, minAreaTrackbar.getValue(), maxAreaTrackbar.getValue());

	cout << "Number of Blob's found: " << blobs.size() << endl;
	for (int i = 0; i < blobs.size(); i++) {
		cout << "Blob nr. " << i + 1 << endl;
		cout << "  First pixel position: (" << blobs[i]->position.x << "," << blobs[i]->position.y << ")" << endl;
		cout << "  Center position: (" << blobs[i]->center.x << "," << blobs[i]->center.y << ")" << endl;
		cout << "  Area: " << blobs[i]->area << endl;
	}
	imshow("Blobs", blobImage);
}

int main(int argc, char** argv) {
	namedWindow("Original Image", WindowFlags::WINDOW_AUTOSIZE);

	// Check presence of required arguments: <image-url>.
	if (argc != 2) {
		if (argc < 2) {
			cout << "To few arguments.";
		} else {
			cout << "To many arguments.";
		}
		cout << " Read " << argc - 1 << ", expected 1. Usage: " << argv[0] << " <image-url>" << endl;
		waitKey();
		return -1;
	}

	//Read supplied image.
	originImage = imread(argv[1], ImreadModes::IMREAD_COLOR);
	if (!originImage.data) {
		cout << "Could not open or find the image" << std::endl;
		waitKey();
		return -1;
	}

	namedWindow("Threshold window", WindowFlags::WINDOW_AUTOSIZE);
	thresholdTrackbar.setup("Threshold window", onTrackbarValueChanged);
	minAreaTrackbar.setup("Threshold window", onTrackbarValueChanged);
	maxAreaTrackbar.setup("Threshold window", onTrackbarValueChanged);

	//Show original image.
	cout << "== Original Image ==" << endl;
	imshow("Original Image", originImage);

	//Wait for any keypress to continue (one of the windows needs to be active).
	waitKey();

	//Exit.
	destroyAllWindows();
	return 0;
}