#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

#include "utils.h"

#include "PR2.h"

using namespace cv;
using namespace std;

bool debug = false;
bool realtime = false;

VideoCapture capture;
double width;
double height;

Trackbar erosionStepsTrackbar("Erosion steps", 0, 10, 4);
Trackbar thresholdTrackbar("Threshold", 0, 255, 88);

int main(int argc, char* argv[]) {
	namedWindow("Original Capture", WindowFlags::WINDOW_AUTOSIZE);

	cout << "Starting external camera..." << endl;
	capture.open(1);

	if (!capture.isOpened()) {
		cout << "No external camera found; using default instead." << endl;
		cout << "Starting internal camera..." << endl;
		capture.open(0);
	}

	if (!capture.isOpened()) {
		cout << "No internal camera found; cannot start program." << endl;
		return -1;
	}

	cout << "Camera started" << endl << "Details:" << endl;
	width = capture.get(VideoCaptureProperties::CAP_PROP_FRAME_WIDTH);
	height = capture.get(VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT);
	cout << "  Resolution: " << width << " x " << height << endl;

	erosionStepsTrackbar.setup("Original Capture");
	thresholdTrackbar.setup("Original Capture");

	//Loop
	cameraLoop();

	//Exit
	cout << "Stopping camera" << endl;
	capture.release();
	destroyAllWindows();
	return 0;
}

void calculateAndShowBlobs(InputArray erodedImage) {
	Mat blobImage;
	Mat coloredImage;

	//Blobs.
	vector<Blob*> blobs;
	Blob::findBlobs(erodedImage, blobImage, blobs);

	//Convert image back from gray to color.
	cvtColor(blobImage, coloredImage, ColorConversionCodes::COLOR_GRAY2BGR);

	putText(coloredImage, "Object count: " + to_string(blobs.size()), Point(10, height - 100), HersheyFonts::FONT_HERSHEY_SIMPLEX, 1, Colors::WHITE);
	for (int i = 0; i < blobs.size(); i++) {
		Blob* blob = blobs[i];
		double radius = Math::areaToRadius(blob->area);
		double smallRadius = 0.3 * radius;
		Scalar color = Colors::CYAN;
		line(coloredImage, blob->center + Point2d(-smallRadius, 0.0), blob->center + Point2d(smallRadius, 0.0), Colors::CYAN, 2);
		line(coloredImage, blob->center + Point2d(0.0, -smallRadius), blob->center + Point2d(0.0, smallRadius), Colors::CYAN, 2);
		circle(coloredImage, blob->center, radius, Colors::CYAN, 2);
	}
	imshow("Blobs", coloredImage);
}

void calculateAndShowRealtimeImages(InputArray originalImage, OutputArray erodedImage) {
	Mat grayedImage;
	Mat binaryImage;

	if (!debug) {
		destroyWindow("Grayscale");
		destroyWindow("Threshold");
		destroyWindow("Erosion");
	}

	//Convert image to gray.
	cvtColor(originalImage, grayedImage, ColorConversionCodes::COLOR_BGR2GRAY);
	if (debug) imshow("Grayscale", grayedImage);

	//Apply a threshold to the image, making it binary.
	threshold(grayedImage, binaryImage, thresholdTrackbar.getValue(), 1, ThresholdTypes::THRESH_BINARY_INV);
	if (debug) imshow("Threshold", binaryImage * 255);

	//Erosion.
	Mat erosionElement = getStructuringElement(MorphShapes::MORPH_RECT, Size(3, 3));
	erode(binaryImage, erodedImage, erosionElement, Point(-1, -1), erosionStepsTrackbar.getValue());
	if (debug) imshow("Erosion", erodedImage.getMat() * 255);
}

void cameraLoop() {
	while (true) {
		Mat originalImage;
		Mat erodedImage;

		//Read camera frame.
		if (!capture.read(originalImage)) {
			cout << "Failed to read frame" << endl;
			return;
		}
		flip(originalImage, originalImage, 1);
		imshow("Original Capture", originalImage);

		//Display fast operations in realtime.
		//Display more heavy operations on a screenshot ('c').
		calculateAndShowRealtimeImages(originalImage, erodedImage);
		if (realtime) calculateAndShowBlobs(erodedImage);

		//Keyboard control; exit by escape.
		switch (waitKey(1)) {
		case 'c':
			calculateAndShowBlobs(erodedImage);
			break;
		case 'd':
			debug = !debug;
			break;
		case 'r':
			realtime = !realtime;
			break;
		case 27:
			return;
		}
	}
}