#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

#include "utils.h"

#include "PR3.h"

using namespace cv;
using namespace std;

bool debug = false;
bool realtime = false;

VideoCapture capture;
double width;
double height;

Trackbar erosionStepsTrackbar("Opening steps", 0, 10, 2);
Trackbar correctionTrackbar("Color", 0, 255, 112);
Trackbar thresholdTrackbar("Threshold", 0, 255, 90);

int main(int argc, char* argv[]) {
	namedWindow("Original Capture", WindowFlags::WINDOW_AUTOSIZE);

	cout << "Starting external camera..." << endl;
	capture.open(1);

	if (!capture.isOpened()) {
		cout << "No external camera found; using default instead." << endl;
		cout << "Starting internal camera..." << endl;
		capture.open(0);
	}

	if (!capture.isOpened()) {
		cout << "No internal camera found; cannot start program." << endl;
		return -1;
	}

	cout << "Camera started" << endl << "Details:" << endl;
	width = capture.get(VideoCaptureProperties::CAP_PROP_FRAME_WIDTH);
	height = capture.get(VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT);
	cout << "  Resolution: " << width << " x " << height << endl;

	erosionStepsTrackbar.setup("Original Capture");
	correctionTrackbar.setup("Original Capture");
	thresholdTrackbar.setup("Original Capture");

	//Loop
	cameraLoop();

	//Exit
	cout << "Stopping camera" << endl;
	capture.release();
	destroyAllWindows();
	return 0;
}

void calculateAndShowBlobs(InputArray erodedImage) {
	
}

void calculateAndShowImages(InputArray originalImage) {
	Mat grayImage;
	Mat hsvImage;
	Mat rangedImage;
	Mat binaryImage;
	Mat erodedImage;
	Mat blobImage;
	Mat resultImage;

	//Remove windows we're not interested in anymore.
	if (!debug) {
		destroyWindow("Gray");
		destroyWindow("HSV");
		destroyWindow("Range");
		destroyWindow("Blur");
		destroyWindow("Erosion");
		destroyWindow("Blobs");
	}

	if (!realtime) {
		destroyWindow("Result");
	}

	//Convert image to gray.
	cvtColor(originalImage, grayImage, ColorConversionCodes::COLOR_BGR2GRAY);

	//Convert image to hsv.
	cvtColor(originalImage, hsvImage, ColorConversionCodes::COLOR_BGR2HSV);
	if (debug) imshow("HSV", hsvImage);

	//Select specific colors from image. Note: doesn't make it (pure) binary.
	HSVColor fromHsv = HSVColor((double)correctionTrackbar.getValue() - 15, 50, 50);
	HSVColor toHsv = HSVColor((double)correctionTrackbar.getValue() + 15, 255, 255);
	inRange(hsvImage, fromHsv, toHsv, rangedImage);
	if (debug) imshow("Range", rangedImage);

	//An extra conversion is needed to go from [0|255] to [0|1] binary type for the blob algorithm.
	Mat binaryHsvImage;
	threshold(rangedImage, binaryHsvImage, 127, 1, ThresholdTypes::THRESH_BINARY);

	//Threshold gray image.
	Mat binaryGrayImage;
	threshold(grayImage, binaryGrayImage, thresholdTrackbar.getValue(), 1, ThresholdTypes::THRESH_BINARY_INV);
	if (debug) imshow("Gray", binaryGrayImage * 255);

	bitwise_and(binaryHsvImage, binaryGrayImage, binaryImage);
	
	//Opening.
	Mat erosionElement = getStructuringElement(MorphShapes::MORPH_RECT, Size(3, 3));
	erode(binaryImage, erodedImage, erosionElement, Point(-1, -1), erosionStepsTrackbar.getValue());
	dilate(erodedImage, erodedImage, erosionElement, Point(-1, -1), erosionStepsTrackbar.getValue());
	if (debug) imshow("Erosion", erodedImage * 255);

	//Find object blobs.
	vector<Blob*> blobs;
	Blob::findBlobs(erodedImage, blobImage, blobs);
	if (debug) imshow("Blobs", blobImage * 255);

	//Construct result image.
	originalImage.copyTo(resultImage);
	putText(resultImage, "Object count: " + to_string(blobs.size()), Point(10, height - 100), HersheyFonts::FONT_HERSHEY_SIMPLEX, 1, Colors::WHITE);
	for (int i = 0; i < blobs.size(); i++) {
		Blob* blob = blobs[i];
		double radius = Math::areaToRadius(blob->area);
		double smallRadius = 0.3 * radius;
		Scalar color = Colors::CYAN;
		line(resultImage, blob->center + Point2d(-smallRadius, 0.0), blob->center + Point2d(smallRadius, 0.0), Colors::CYAN, 2);
		line(resultImage, blob->center + Point2d(0.0, -smallRadius), blob->center + Point2d(0.0, smallRadius), Colors::CYAN, 2);
		circle(resultImage, blob->center, radius, Colors::CYAN, 2);
	}
	if (realtime) imshow("Result", resultImage);
}

void cameraLoop() {
	while (true) {
		Mat originalImage;
		Mat erodedImage;

		//Read camera frame.
		if (!capture.read(originalImage)) {
			cout << "Failed to read frame" << endl;
			return;
		}

		//Flip the image to make a mirror effect.
		flip(originalImage, originalImage, 1);
		imshow("Original Capture", originalImage);

		calculateAndShowImages(originalImage);

		//Keyboard control; exit by escape.
		switch (waitKey(1)) {
		case 'c':
			calculateAndShowBlobs(erodedImage);
			break;
		case 'd':
			debug = !debug;
			break;
		case 'r':
			realtime = !realtime;
			break;
		case 27: //Escape
			return;
		}
	}
}